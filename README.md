# Mac-VSync-BeamSync-Switcher
Turn VSync(BeamSync) on and off. In the latest Mac OS update, BeamSync is forced on all applications which causes games such as CSGO to be locked to 60 FPS.
