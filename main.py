from Tkinter import *
import ctypes, ctypes.util

c_CoreGraphics = ctypes.CDLL(ctypes.util.find_library('CoreGraphics'))

master = Tk()

def VsyncOn():
    c_CoreGraphics.CGSSetDebugOptions(0)
    c_CoreGraphics.CGSDeferredUpdates(1)

def VsyncOff():
    c_CoreGraphics.CGSSetDebugOptions(ctypes.c_uint64(0x08000000))
    c_CoreGraphics.CGSDeferredUpdates(0)

w = 500
h = 100

f = Frame(master, width=w).pack()
master.title("Mac VSync Switcher 1.0")

ws = master.winfo_screenwidth()
hs = master.winfo_screenheight()

x = (ws/2) - (w/2)
y = (hs/2) - (h/2)

master.geometry('%dx%d+%d+%d' % (w, h, x, y))

master.focus_set()

b = Button(f, command = VsyncOn, text="VSync on", anchor=W, justify=CENTER, padx=2).pack(fill=BOTH, expand=1)
b2 = Button(f, command = VsyncOff, text="VSync off", anchor=W, justify=CENTER, padx=2).pack(fill=BOTH, expand=1)


mainloop()
